/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;

/**
 *
 * @author lendle
 */
public class DictClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Socket socket = new Socket("dict.org", 2628);
        OutputStream outputStream = socket.getOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "utf-8");
        writer.write("define wn cat\r\n");
        writer.flush();
        
        InputStream inputStream = socket.getInputStream();
        StringBuilder sb =new StringBuilder();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream,"utf-8");
        BufferedReader reader = new BufferedReader(inputStreamReader);
        while (true) {            
            String str = reader.readLine();
            if (".".equals(str)){
                break;
            }
            sb.append(str);
            sb.append("\r\n");
        }
        System.out.println(sb);
        writer.write("quit\r\n");
        writer.flush();
        socket.close();
    }

}
